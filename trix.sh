#!/bin/bash

##################################################
#                                                #
#   Author: Beau McAdams (ixp123)                #
#   License: WTFPL-2                             #
#                                                #
#   And most of all, enjoy!                      #
#                                                #
##################################################




##
## Extra variables
##
exitSay="Please enter the neovim config at ~/.config/neovim/init.vim, and enter the PlugInstall command"


##
## Dependencies
##

# Install and configure opendoas as a secure replacement for sudo
sudo pacman -S opendoas
sudo touch /etc/doas.conf
echo "permit nopass $USER as root" > /etc/doas.conf

# Set dependencies
depends="xorg xorg-xinit git"

# Install dependencies
doas pacman -S --noconfirm $depends



##
## Paru AUR Helper
##

mkdir ~/.github
cd ~/.github

git clone https://aur.archlinux.org/paru.git
cd paru

makepkg -si --noconfirm

cd ..



##
## Cloning dotfiles
##

dots=https://gitlab.com/ixp123/dotfiles

git clone $dots

cd dotfiles

# Copying dotfiles
cp -r .config ~/.config
cp .zshrc ~/.zshrc
cp .bashrc ~/.bashrc
cp .Xresources ~/.Xresources
cp .Xresources ~/.Xdefaults

cd ~/.config/bspwm
doas chmod +x bspwmrc
cd ~/.config/sxhkd
doas chmod +x sxhkdrc

# Cloning wallpapers
cd ~
git clone https://github.com/FrenzyExists/wallpapers Wallpapers

# Configuring zsh
mkdir ~/.zsh
cd ~/.zsh
shplug1="https://github.com/zsh-users/zsh-syntax-highlighting"
shplug2="https://github.com/zsh-users/zsh-autosuggestions"
shplug3="https://github.com/zsh-users/zsh-completions"

git clone $shplug1
git clone $shplug2
git clone $shplug3

cd ~/.github

# Installing ly display manager
dispdm="https://github.com/fairyglade/ly.git"
cd ly
make
doas make install

cd ~

# Installing Starship
sh -c "$(curl -fsSL https://starship.rs/install.sh)"
echo "eval "$(starship init zsh)" " > ~/.zshrc

# Installing vim-plug
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim



##
## Installing packages
##

packs="bspwm sxhkd ttf-liberation python-pywal python-pyalsa pulseaudio pipewire-pulse pulsemixer 
redshift htop vim neovim qalculate-gtk kitty nodejs moc"

parupacks="polybar-git ttf-nerd-fonts-hack-complete-git nerd-fonts-source-code-pro librewolf-bin
ranger-git drb-git ripcord moc-pulse"

doas pacman -S --noconfirm $packs

paru -S --noconfirm $parupacks




# Configure ~/.xinitrc
echo "#!/bin/sh" > ~/.xinitrc
echo "exec bspwm" > ~/.xinitrc

cd ~




echo $exitSay
